#define IR_BUNNY 2
#define BUNNY_RED 3
#define BUNNY_GREEN 8
#define IR_DEER 4
#define DEER_RED 5
#define DEER_GREEN 9
#define IR_LION 6
#define LION_GREEN 7
#define SOLENOID 10
#define IR_BUNNY_HELPER 11
#define IR_DEER_HELPER 12
#define IR_LION_HELPER 13

bool hunted_deer = false;
bool hunted_bunny = false;
bool hunted_lion = false;

int i = 0;

void setup() {
  pinMode(IR_BUNNY, INPUT);
  pinMode(IR_DEER, INPUT);
  pinMode(IR_LION, INPUT);
  pinMode(BUNNY_RED, OUTPUT);
  pinMode(BUNNY_GREEN, OUTPUT);
  pinMode(DEER_GREEN, OUTPUT);
  pinMode(DEER_RED, OUTPUT);
  pinMode(LION_GREEN, OUTPUT);
  pinMode(SOLENOID, OUTPUT);
  pinMode(IR_BUNNY_HELPER, INPUT);
  pinMode(IR_DEER_HELPER, INPUT);
  pinMode(IR_LION_HELPER, INPUT);
  turnOff();
}

void turnOff(){
  digitalWrite(BUNNY_GREEN, LOW);
  digitalWrite(BUNNY_RED, LOW);
  digitalWrite(DEER_RED, LOW);
  digitalWrite(DEER_GREEN, LOW);
  digitalWrite(LION_GREEN, LOW);
  digitalWrite(SOLENOID, LOW);
}

void(* resetFunc) (void) = 0;

void restartGame() {
  resetFunc();
}

void blinkLed(int led) {
  for (int i = 0; i < 3; i++) {
    digitalWrite(led, HIGH);
    delay(200);
    digitalWrite(led, LOW);
    delay(200);
  }
}

void blinkDeerBunny() {
  digitalWrite(BUNNY_GREEN, LOW);
  digitalWrite(DEER_GREEN, LOW);
  for (int i = 0; i < 3; i++) {
    digitalWrite(BUNNY_RED, HIGH);
    digitalWrite(DEER_RED, HIGH);
    delay(200);
    digitalWrite(BUNNY_RED, LOW);
    digitalWrite(DEER_RED, LOW);
    delay(200);
  }
}

void loop() {
  if(pulseIn(IR_BUNNY, HIGH) || pulseIn(IR_BUNNY_HELPER, HIGH)) {
    hunted_bunny = true;
    digitalWrite(BUNNY_GREEN, HIGH);
    i = 0;
  }
  
  else if((pulseIn(IR_LION, HIGH) || pulseIn(IR_LION_HELPER, HIGH)) && !hunted_deer && hunted_bunny) {
   digitalWrite(BUNNY_GREEN, LOW);
   blinkLed(BUNNY_RED);
   restartGame();
  }
  
  else if((pulseIn(IR_DEER, HIGH) || pulseIn(IR_DEER_HELPER, HIGH)) && hunted_bunny) {
    hunted_deer = true;
    digitalWrite(DEER_GREEN, HIGH);
    i = 0;
  }
  
  else if((pulseIn(IR_LION, HIGH) || pulseIn(IR_LION_HELPER, HIGH)) && hunted_bunny && hunted_deer) {
    hunted_lion = true;
    digitalWrite(LION_GREEN, HIGH);
    digitalWrite(SOLENOID, HIGH);
    i = 0;
  }
  else {
    i++;
    if (i % 5 == 0){
      if(hunted_lion) {
        restartGame();
      } else if (hunted_bunny && hunted_deer) {
        blinkDeerBunny();
        restartGame();
      } else if (hunted_bunny){
        digitalWrite(BUNNY_GREEN, LOW);
        blinkLed(BUNNY_RED);
        restartGame();
      }
      else { restartGame(); }
    }
  }
}
